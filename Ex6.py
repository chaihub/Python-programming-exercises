# code example 6
import math
C = 50; H = 30
print('C is', C, 'H is', H)
print('Enter numbers separated by commas')
inputs = input().split(',')
#print('*'.join(inputs))
outputs = [math.sqrt(100*int(x)/30) for x in inputs]
for i in outputs:
    print('{0:>+6.1f}'.format(i))